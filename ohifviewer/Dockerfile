# multistage build
FROM node:8 as builder
LABEL maintainer "LibreHealth Infrastructure Team <infrastructure@librehealth.io>"

# Create user meteor who will run all entrypoint instructions
RUN useradd meteor -G staff -m -s /bin/bash

WORKDIR /home/meteor
ENV METEOR_DEBUG_BUILD="1"
USER meteor
RUN (curl https://install.meteor.com/ | sh)

RUN git clone https://gitlab.com/sunbiz/lh-radiology.git \
&& cd lh-radiology/Viewers/OHIFViewer \
&& sed -i "s/localhost\:8042/radiology.librehealth.io:8042/g" ../config/orthancDICOMWeb.json
WORKDIR /home/meteor/lh-radiology/Viewers/OHIFViewer
RUN npm install \
&& METEOR_PACKAGE_DIRS="../Packages" \
meteor build --directory $HOME/build --architecture os.linux.x86_64
RUN cd $HOME/build/bundle/programs/server && npm install --production

# final image
FROM node:8
LABEL maintainer "LibreHealth Infrastructure Team <infrastructure@librehealth.io>"
RUN mkdir /app && chown node:node /app
RUN npm install -g pm2
    COPY config.json /app
WORKDIR /app
COPY --chown=node:node --from=builder /home/meteor/build/bundle /app
USER node
CMD ["pm2-runtime", "/app/config.json"]
